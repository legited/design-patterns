import {
    GifServiceFactory,
    GifProviders
} from '@vladrmnv/gif-service';



export default (async function main() {
    const gifServiceFactory = new GifServiceFactory();
    const gifService = gifServiceFactory.get(GifProviders.Giphy);
    const gifUrl = await gifService.getRandomGifUrl('cat');
    console.log(gifUrl);
})();