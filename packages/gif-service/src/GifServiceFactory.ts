import { Giphy } from "./Giphy";

export enum GifProviders {
    Giphy = 'Giphy'
}

export class GifServiceFactory {
    get(service: GifProviders) {
        switch (service) {
            case GifProviders.Giphy:
                return new Giphy();
        }
    }
}