export interface GifService {
    getRandomGifUrl(tag: string): Promise<string>
};
