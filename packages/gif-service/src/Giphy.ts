import { GifService } from './GifService';
import {
    get,
    Options
} from 'request-promise';
const API_KEY = '1zwEcFWLE0WMxskdpttWxnWn6Dmzvz0V';

export class Giphy implements GifService {
    readonly baseUrl = 'https://api.giphy.com';

    async getRandomGifUrl(tag?: string): Promise<string> {
        const endpoint = '/v1/gifs/random'
        const url = this.baseUrl + endpoint;
        const options: Options = {
            url,
            qs: {
                api_key: API_KEY,
                tag
            },
            json: true
        }
        return get(options)
            .then(res => res.data.url);
    }
}
